<?php

/**
 * Logger
 */
class Logger extends Exception
{
    public $errore;
    private $dataFile;
    private $data_ita;
    private $expiry_day_log;

    public function __construct()
    {
        $this->dataFile = date("Y-m-d");
        $this->data_ita = date("d-m-Y H:i:s");
        $this->expiry_day_log = EXPIRY_DAY_LOG;
    }

    /**
     * @param string $controller Nome da dare alla cartella
     * @param int $type Indica lo stato del log es. inzio, fine 
     * @param string $titolo Titolo
     * @param string $descrizione eventuale descrizione
     * 
     * @return [type]
     * 
     * Created at: 28/1/2023, 19:29:07 (Europe/Rome)
     * @author     Salvatore Sellaroli 
     */
    public function scrivi(string $controller, int $type, string $titolo = '', string $descrizione = '')
    {

        try {

            $dimensioneFileMax = (int) DIMENSIONE_FILE_LOG;

            $countLog = 0;

            // LOG/[data]/[nome controller]

            $dir = PATH_LOG . '/' . $this->dataFile  . '/' . $controller . '/';

            # Se la cartella non è esiste viene creata
            if (!file_exists($dir)) {
                //mkdir($dir, 0777);
                if (!mkdir($dir, 0777, true)) {
                    die('Errore creazione cartella'. $dir);
                }
            }

            # Verifica se gia è presente un file con lo stesso nome altrimenti ne crea uno nuovo
            for ($i = 0; $i <= $countLog; $i++) {

                # Nome del file 
                $nomeFile = $controller . '_' . $countLog . '.log';

                if (file_exists($dir . $nomeFile)) {

                    $size =  $this->fileSize($dir . $nomeFile);

                    if ($size >= $dimensioneFileMax) {

                        $countLog = $countLog + 1;
                    }
                }
            }

            $br = '==========================================================================================================================';

            $fp = fopen($dir . $nomeFile, "a+");

            # Start =  Header del log
            if ($type === 1) {

                fwrite($fp, $br . "\r\n");

                fwrite($fp, "                                     " . $titolo . "\r\n");

                fwrite($fp, "[" . $this->data_ita . "]  -  LOG: " . $controller . "\r\n");

                fwrite($fp,  "[" . $this->data_ita . "]  -  IP: " . IP_CLIENT . "\r\n");

                fwrite($fp,  "[" . $this->data_ita . "]  -  HOSTNAME: " . HOSTNAME . "\r\n");

                fwrite($fp,  "[" . $this->data_ita . "]  -  USER_AGENT: " . USER_AGENT . "\r\n");

                fwrite($fp, "**************************************************************************************************************************\r\n");
            }

            # Middle = Corpo del log
            if ($type === 2) {

                fwrite($fp, "[" . $this->data_ita . "] - " . $titolo . " \r\n");
                fwrite($fp, $descrizione . "\r\n");
            }

            # End =  Chiusura del log
            if ($type === 3) {

                fwrite($fp, $br . "\r\n");
            }

            fclose($fp);

            # Elimina i log piu vecchi 
            $this->svuotaCartella(PATH_LOG . '/');
        } catch (Exception $ex) {

            throw new Exception($ex->getMessage());

            $dir = PATH_LOG . '/errore_log/';

            $fp = fopen($dir . $nomeFile, "a+");

            fwrite($fp, "[" . $this->data_ita . "]  - errore scrittura log \r\n");

            fwrite($fp, $ex->getMessage() . "\r\n");

            fwrite($fp, $br . "\r\n");

            fclose($fp);
        }
    }
    protected function fileSize(string $file)
    {

        $filesize = filesize($file);
        $fileKB = round(($filesize / 1024), 2);

        return $fileKB;
    }

    protected function svuotaCartella($dirpath)
    {
        $gg = (string) "-" . $this->expiry_day_log . " days";

        $date = date('Y-m-d', strtotime($gg));

        $handle = opendir($dirpath);

        while (($file = readdir($handle)) !== false) {

            if ($file != '.' && $file != '..' && $file < $date) {

                $files = glob($dirpath . $file . '/*'); //get all file names

                foreach ($files as $filea) {
                    if (is_file($filea))
                        unlink($filea); //delete file
                }

                rmdir($dirpath . $file);
            }
        }

        closedir($handle);
    }
}
abstract class logType
{
    const start = 1;
    const body = 2;
    const end = 3;
}
