<?php
class Email
{
    private $mail; 
    public $dati;
    public $errore;

    public function __construct($mail)
    {
        $this->mail = $mail;
    }

    public function email(string $oggetto, string $email): bool
    {
        try {


            $toBody = <<<HTML
                <html>
                <head>
                <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui, viewport-fit=cover">
                <style>
                body{
                    text-rendering: optimizeLegibility;
                }
                </style>
                </head>
                <body>
                <h3>INSERIRE MESSAGGIO EMAIL!</h3>
                <br>
                <br>
                <b><small>NOME_APP</small></b>
                </body>
                </html>
            HTML;

            $mail = $this->mail;
            $mail->IsSMTP();
            $mail->SMTPAuth = true;
            $mail->SMTPSecure = EMAIL_SMTP;
            $mail->Host = EMAIL_HOST;
            $mail->Port = EMAIL_PORT;
            $mail->IsHTML(true);
            $mail->Username = $_ENV["username_email"];
            $mail->Password = $_ENV["password_email"];
            $mail->setFrom(EMAIL_TO, NOME_APP);
            $mail->AddAddress($email);
            $mail->Subject = $oggetto;
            $mail->Body = $toBody;
            if (!$mail->send()) {
                throw new Exception($mail->ErrorInfo);
            }
            return true;
        } catch (Exception $ex) {

            return false;
        }
    }
}
