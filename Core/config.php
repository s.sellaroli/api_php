<?php
# Setting LOG
define("PATH_LOG", dirname(__DIR__, 1) . '/LOG'); # Percorso Cartella
define("EXPIRY_DAY_LOG", 6); # Giorni di conservazione dei log
define("DIMENSIONE_FILE_LOG", 9664); # Dimensione singolo file log
# Setting Email
define("EMAIL_HOST", "");
define("EMAIL_TO", "");
define("EMAIL_PORT", "587");
define("EMAIL_SMTP", "TLS");
# Costanti
define("NOME_APP", "NOME_APP 2");
define("IP_CLIENT", $_SERVER['REMOTE_ADDR']);
define("HOSTNAME", gethostbyaddr($_SERVER['REMOTE_ADDR']));
define("USER_AGENT", $_SERVER['HTTP_USER_AGENT']);
define("NOME_SERVER", $_SERVER["SERVER_NAME"]);

