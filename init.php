<?php
# Caricamento file config
require_once __DIR__.'/Core/config.php';

# Autoloader Classi lib
require __DIR__ . '/Core/libs/vendor/autoload.php';

# Libreria per gestire file .env
$dotenv = Dotenv\Dotenv::createImmutable(__DIR__)->load();

# Init PHPMailer
use PHPMailer\PHPMailer\PHPMailer;
$mail = new PHPMailer(true);

// Autoloader Classi
function includeClassFile($class)
{
    $file = __DIR__."/Core/classes/" . strtolower(trim($class)) . ".php";
    if (file_exists($file)) {
        // Il file esiste, lo includiamo
        include $file;
        return true;
    }
    return false;
}

spl_autoload_register('includeClassFile');

// Inizializzazione Classi comuni
$log = new Logger();
$sendMail = new Email($mail);